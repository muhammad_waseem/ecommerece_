package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.dhcollator.ecommerce.dtos.MyAddsDTO;
import com.dhcollator.ecommerce.dtos.request.AddPostRequest;
import com.dhcollator.ecommerce.models.BaseModel;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.AddProductResponse;
import com.dhcollator.ecommerce.response.EmptyResponse;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddPostActivity extends AppCompatActivity implements View.OnClickListener, INetworkListener, Constants {

    EditText titleET;
    EditText priceET;
    EditText locationET;
    EditText brandET;
    EditText yearET;
    EditText conditionET;
    EditText contactET;
    EditText detailET;
    ProgressDialog progressDialog;
    Button saveAddIV;
    Button updateAddIV;
    ImageView capturePic;
    private static int RESULT_LOAD_GALLERY = 1;
    private static int RESULT_LOAD_CAMERA = 0;
    boolean isEdit = false;
    MyAddsDTO detailData;
    // int productId;
    String picturePath;
    int productAddedId;
    int imageCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        setTitle("Post Ad");
        titleET = (EditText) findViewById(R.id.et_add_post_title);
        priceET = (EditText) findViewById(R.id.et_add_post_price);
        locationET = (EditText) findViewById(R.id.et_add_post_location);
        brandET = (EditText) findViewById(R.id.et_add_post_brand);
        yearET = (EditText) findViewById(R.id.et_add_post_year);
        conditionET = (EditText) findViewById(R.id.et_add_post_condition);
        contactET = (EditText) findViewById(R.id.et_add_post_contact);
        detailET = (EditText) findViewById(R.id.et_add_post_detail);
        saveAddIV = (Button) findViewById(R.id.iv_add_post_save);
        updateAddIV = (Button) findViewById(R.id.iv_add_post_update);
        capturePic = (ImageView) findViewById(R.id.iv_select_pic);
        capturePic.setOnClickListener(this);
        saveAddIV.setOnClickListener(this);
        updateAddIV.setOnClickListener(this);
        progressDialog = new ProgressDialog(AddPostActivity.this);


        boolean isEdit = getIntent().getBooleanExtra("edit", false);

        if (isEdit) {
            updateAddIV.setVisibility(View.VISIBLE);
            detailData = (MyAddsDTO) getIntent().getSerializableExtra("editData");
            Log.d("detail-is", "" + detailData.getStatus());
            titleET.setText(detailData.getName());
            priceET.setText(detailData.getPrice());
            locationET.setText(detailData.getLocation());
            brandET.setText(detailData.getBrand());
            yearET.setText(detailData.getCreated_at());
            conditionET.setText(detailData.getPresent_condition());
            contactET.setText(detailData.getContact());
            detailET.setText(detailData.getDetail());
        } else {
            saveAddIV.setVisibility(View.VISIBLE);
        }

        findViewById(R.id.iv_add_post_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
        findViewById(R.id.iv_add_post_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });
        findViewById(R.id.iv_select_pic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageCounter < 3) {
                    selectImage();
                } else {
                    Snackbar.make(v, "You cannot select images more than Three", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

    }


    public void updateData() {
        if (validateFields()) {
            int userId = SharedPreferenceUtils.getInstance(AddPostActivity.this).getUserId();
            String action = Constants.ACTION_UPDATE_PRODUCT;

            String name = titleET.getText().toString();
            String price = priceET.getText().toString();
            String location = locationET.getText().toString();
            String brand = brandET.getText().toString();
            String year = yearET.getText().toString();
            String condition = conditionET.getText().toString();
            String detail = detailET.getText().toString();
            String contact = contactET.getText().toString();

            AddPostRequest request = new AddPostRequest();

            request.setUserId(userId);
            request.setStatus("active");
            request.setName(name);
            request.setPrice(price);
            request.setLocation(location);
            request.setBrand(brand);
            request.setCondition(condition);
            request.setDetail(detail);
            request.setCode("");
            request.setContact(contact);

            NetworkOperations.getInstance().postData(AddPostActivity.this, action, request, this, BaseModel.class);
        }
    }


    public void saveData() {
        if (validateFields()) {
            int userId = SharedPreferenceUtils.getInstance(AddPostActivity.this).getUserId();
            String action = Constants.ACTION_POST_ADDS_PRODUCTS;

            String name = titleET.getText().toString();
            String price = priceET.getText().toString();
            String location = locationET.getText().toString();
            String brand = brandET.getText().toString();
            String year = yearET.getText().toString();
            String condition = conditionET.getText().toString();
            String detail = detailET.getText().toString();
            String contact = contactET.getText().toString();

            AddPostRequest request = new AddPostRequest();

            request.setUserId(userId);
            request.setStatus("active");
            request.setName(name);
            request.setPrice(price);
            request.setLocation(location);
            request.setBrand(brand);
            request.setCondition(condition);
            request.setDetail(detail);
            request.setCode("");
            request.setContact(contact);


            NetworkOperations.getInstance().postData(AddPostActivity.this, action, request, this, AddProductResponse.class);
        }
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("posting...");
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object obj) {
        progressDialog.cancel();
        if (obj instanceof AddProductResponse) {
            AddProductResponse result = (AddProductResponse) obj;
            if (result.isSuccess()) {

                productAddedId = result.getData().get(0).getId();
                Toast.makeText(AddPostActivity.this, result.getMessage(),
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AddPostActivity.this, Dashboard.class);
                startActivity(intent);

                uploadFileToS3(picturePath, productAddedId);

            } else {
                Toast.makeText(AddPostActivity.this, "failed to post product",
                        Toast.LENGTH_LONG).show();
            }
        }
        if (obj instanceof BaseModel) {
            BaseModel result = (BaseModel) obj;
            if (result.isSuccess()) {
                Intent intent = new Intent(AddPostActivity.this, Dashboard.class);
                startActivity(intent);
            } else {
                Toast.makeText(AddPostActivity.this, "failed to update",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private void selectImage() {
        final CharSequence[] items = {"Choose from Gallery", "Take Picture",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddPostActivity.this);
        builder.setTitle("Choose Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Choose from Gallery")) {
                    imageCounter++;
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, RESULT_LOAD_GALLERY);
                } else if (items[item].equals("Take Picture")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, RESULT_LOAD_CAMERA);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void doInBackground() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_CAMERA && resultCode == RESULT_OK && null != data) {
            Bitmap image = (Bitmap) data.getExtras().get("data");

            capturePic.setImageBitmap(image);

        }

        if (requestCode == RESULT_LOAD_GALLERY && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            //   uploadFileToS3(picturePath);
            capturePic.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    private void uploadFileToS3(String filePath, int productId) {

        AWSCredentials credential = new BasicAWSCredentials(S3_ACCESS_KEY_ID, S3_SECRET_KEY);
        AmazonS3 s3 = new AmazonS3Client(credential);
        TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());

        String productId1 = String.valueOf(productId);
        if (productId1 != null && productId1.length() > 0 && filePath != null) {
            TransferObserver observer1 = transferUtility.upload(
                    S3_BUCKET_NAME,
                    productId1 + "_product",
                    new File(filePath)
            );
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddPostActivity.this, Dashboard.class);
        startActivity(intent);
        finish();
    }

    private boolean validateFields() {
        boolean result = true;

        String name = titleET.getText().toString();
        String price = priceET.getText().toString();
        String location = locationET.getText().toString();
        String brand = brandET.getText().toString();
        String year = yearET.getText().toString();
        String condition = conditionET.getText().toString();
        String detail = detailET.getText().toString();
        String contact = contactET.getText().toString();

        if (TextUtils.isEmpty(name)) {
            titleET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(price)) {
            priceET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(location)) {
            locationET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(brand)) {
            brandET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(year)) {
            yearET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(condition)) {
            conditionET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(contact)) {
            contactET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(detail)) {
            detailET.setError("this field is required");
            result = false;
        }
        return result;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
