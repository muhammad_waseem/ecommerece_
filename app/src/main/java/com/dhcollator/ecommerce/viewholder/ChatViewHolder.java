package com.dhcollator.ecommerce.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhcollator.ecommerce.R;

/**
 * Created by Talha Hafeez on 5/23/2015.
 */
public class ChatViewHolder extends RecyclerView.ViewHolder {

    private TextView messageIn;
    private TextView messageOut;
    private TextView from;
    private TextView me;
    private TextView yourTime;
    private TextView meTime;
    ImageView meIV;
    ImageView yourIV;
    private LinearLayout containerIn;
    private LinearLayout containerOut;

    public ChatViewHolder(View row) {
        super(row);

        messageIn = (TextView) row.findViewById(R.id.me_message);
        messageOut = (TextView) row.findViewById(R.id.your_message);
        from = (TextView) row.findViewById(R.id.your_name);
        me = (TextView) row.findViewById(R.id.me_name);
        yourTime = (TextView) row.findViewById(R.id.your_time_send);
        meTime = (TextView) row.findViewById(R.id.me_time_send);
        meIV = (ImageView) row.findViewById(R.id.iv_me_photo);
        yourIV = (ImageView) row.findViewById(R.id.iv_your_photo);
        containerIn = (LinearLayout) row.findViewById(R.id.my_layout);
        containerOut = (LinearLayout) row.findViewById(R.id.your_layout);
    }

    public ImageView getYourIV() {
        return yourIV;
    }

    public void setYourIV(ImageView yourIV) {
        this.yourIV = yourIV;
    }

    public ImageView getMeIV() {
        return meIV;
    }

    public void setMeIV(ImageView meIV) {
        this.meIV = meIV;
    }
    public TextView getYourTime() {
        return yourTime;
    }

    public void setYourTime(TextView yourTime) {
        this.yourTime = yourTime;
    }

    public TextView getMeTime() {
        return meTime;
    }

    public void setMeTime(TextView meTime) {
        this.meTime = meTime;
    }

    public TextView getMessageIn() {
        return messageIn;
    }

    public TextView getMessageOut() {
        return messageOut;
    }

    public TextView getFrom() {
        return from;
    }

    public TextView getMe() {
        return me;
    }

//    public ImageView getIcon() {
//        return icon;
//    }

    public LinearLayout getContainerIn() {
        return containerIn;
    }

    public LinearLayout getContainerOut() {
        return containerOut;
    }
}

