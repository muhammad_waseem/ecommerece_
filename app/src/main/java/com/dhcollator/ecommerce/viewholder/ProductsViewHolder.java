package com.dhcollator.ecommerce.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhcollator.ecommerce.R;

import java.text.BreakIterator;

/**
 * Created by Lenovo on 6/25/2016.
 */
public class ProductsViewHolder extends RecyclerView.ViewHolder {


   public CardView productsDashboardContainerCV;
    public TextView titleTV;
    public TextView priceTV;
    public TextView locationTV;
    public ImageView productPicIV;
    public TextView chatTV;


    public ProductsViewHolder(View itemView) {
        super(itemView);
        productsDashboardContainerCV = (CardView) itemView.findViewById(R.id.cv_row_dashboard_container);
        titleTV = (TextView) itemView.findViewById(R.id.tv_dashboard_title);
        priceTV = (TextView) itemView.findViewById(R.id.tv_dashboard_price);
        locationTV = (TextView) itemView.findViewById(R.id.tv_dashboard_location);
        chatTV = (TextView) itemView.findViewById(R.id.tv_dashboard_chat);
        productPicIV = (ImageView) itemView.findViewById(R.id.iv_dashboard_product_pic);
    }

    public TextView getTitleTV() {
        return titleTV;
    }

    public void setTitleTV(TextView titleTV) {
        this.titleTV = titleTV;
    }

    public CardView getProductsDashboardContainerCV() {
        return productsDashboardContainerCV;
    }

    public void setProductsDashboardContainerCV(CardView productsDashboardContainerCV) {
        this.productsDashboardContainerCV = productsDashboardContainerCV;
    }

    public TextView getPriceTV() {
        return priceTV;
    }

    public void setPriceTV(TextView priceTV) {
        this.priceTV = priceTV;
    }

    public TextView getLocationTV() {
        return locationTV;
    }

    public void setLocationTV(TextView locationTV) {
        this.locationTV = locationTV;
    }

    public ImageView getProductPicIV() {
        return productPicIV;
    }

    public void setProductPicIV(ImageView productPicIV) {
        this.productPicIV = productPicIV;
    }

    public TextView getChatTV() {
        return chatTV;
    }

    public void setChatTV(TextView chatTV) {
        this.chatTV = chatTV;
    }

}
