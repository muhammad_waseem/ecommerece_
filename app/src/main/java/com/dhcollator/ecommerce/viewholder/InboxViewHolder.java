package com.dhcollator.ecommerce.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhcollator.ecommerce.R;

/**
 * Created by Lenovo on 8/10/2016.
 */
public class InboxViewHolder extends RecyclerView.ViewHolder {

    public CardView inboxCV;
    public TextView nameSenderTV;
    public TextView lastMessageTV;
    ImageView profilePicIV;

    public InboxViewHolder(View itemView) {
        super(itemView);
        inboxCV = (CardView) itemView.findViewById(R.id.cv_inbox);
        nameSenderTV = (TextView) itemView.findViewById(R.id.tv_inbox_person_name_);
        lastMessageTV = (TextView) itemView.findViewById(R.id.tv_inbox_last_message_);
        profilePicIV = (ImageView) itemView.findViewById(R.id.iv_inbox_person_photo);

    }

    public ImageView getProfilePicIV() {
        return profilePicIV;
    }

    public void setProfilePicIV(ImageView profilePicIV) {
        this.profilePicIV = profilePicIV;
    }
    public CardView getInboxCV() {
        return inboxCV;
    }

    public void setInboxCV(CardView inboxCV) {
        this.inboxCV = inboxCV;
    }
    public TextView getNameSenderTV() {
        return nameSenderTV;
    }

    public void setNameSenderTV(TextView nameSenderTV) {
        this.nameSenderTV = nameSenderTV;
    }

    public TextView getLastMessageTV() {
        return lastMessageTV;
    }

    public void setLastMessageTV(TextView lastMessageTV) {
        this.lastMessageTV = lastMessageTV;
    }
}
