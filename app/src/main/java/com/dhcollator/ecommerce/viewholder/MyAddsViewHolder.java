package com.dhcollator.ecommerce.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhcollator.ecommerce.R;

/**
 * Created by Lenovo on 6/25/2016.
 */
public class MyAddsViewHolder extends RecyclerView.ViewHolder {


    public CardView containerCV;
    public TextView titleTV;
    public TextView priceTV;
    public TextView locationTV;
    public Button editIV;
    public Button deleteIV;
    public ImageView myAddPicIV;


    public MyAddsViewHolder(View itemView) {
        super(itemView);
        containerCV = (CardView) itemView.findViewById(R.id.cv_row_my_adds_container);
        titleTV = (TextView) itemView.findViewById(R.id.tv_my_adds_title);
        priceTV = (TextView) itemView.findViewById(R.id.tv_my_adds_price);
        locationTV = (TextView) itemView.findViewById(R.id.tv_my_adds_location);
        editIV = (Button) itemView.findViewById(R.id.btn_my_adds_edit);
        deleteIV = (Button) itemView.findViewById(R.id.btn_my_adds_delete);
        myAddPicIV = (ImageView) itemView.findViewById(R.id.iv_my_adds_pic);
    }

    public ImageView getMyAddPicIV() {
        return myAddPicIV;
    }

    public void setMyAddPicIV(ImageView myAddPicIV) {
        this.myAddPicIV = myAddPicIV;
    }
}
