package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.dhcollator.ecommerce.models.BaseModel;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.EmptyResponse;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;
import com.dhcollator.ecommerce.utils.Utility;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, INetworkListener {
    private EditText mPasswordET;
    private EditText mFirstNameET;
    private EditText mLastNameET;
    private EditText mAgeET;
    private EditText mEmailET;
    private Button signUp;
    ImageView backIV;
    String emailPattern;
    ProgressDialog progressDialog;
    String fName, lName, email, pwd;
    int age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        resourceInit();
        progressDialog = new ProgressDialog(SignUpActivity.this);
        signUp.setOnClickListener(this);
        backIV.setOnClickListener(this);
    }

    private void resourceInit() {

        mFirstNameET = (EditText) findViewById(R.id.et_register_fname);
        mLastNameET = (EditText) findViewById(R.id.et_register_lname);
        mAgeET = (EditText) findViewById(R.id.et_register_age);
        mEmailET = (EditText) findViewById(R.id.et_register_email);
        mPasswordET = (EditText) findViewById(R.id.et_register_password);
        signUp = (Button) findViewById(R.id.btn_register_submit);
        backIV = (ImageView) findViewById(R.id.iv_register_back);
    }

    private void registerUser() {
        if (isValidInput()) {
            int age = Integer.parseInt(mAgeET.getText().toString());
            UserDTO1 user = new UserDTO1();
            user.setFirstName(fName);
            user.setLastName(lName);
            user.setEmail(email);
            user.setPassword(pwd);
            user.setAge(age);
            if (fName != null && email != null && pwd != null) {
                NetworkOperations.getInstance().registration(this, user, this);
            } else {
                Toast.makeText(SignUpActivity.this, "Fields are required", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isValidInput() {
        boolean result = true;

        fName = mFirstNameET.getText().toString();
        lName = mLastNameET.getText().toString();
        email = mEmailET.getText().toString();
        pwd = mPasswordET.getText().toString();
        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (TextUtils.isEmpty(fName)) {
            mFirstNameET.setError("this field is required");
            result = false;
        }
        if (TextUtils.isEmpty(lName)) {
            mLastNameET.setError("this field is required");
            result = false;
        }
//        if (TextUtils.isEmpty(email)) {
//            mEmailET.setError("this field is required");
//            result = false;
//        }
        if (TextUtils.isEmpty(pwd)) {
            mPasswordET.setError("this field is required");
            result = false;
        }
        if (!TextUtils.isEmpty(email)) {
            if (email.matches(emailPattern)) {
            } else {
                mEmailET.setError("please enter valid email");
                result = false;
            }
        } else {
            mEmailET.setError("Mandatory field");
            result = false;
        }
        return result;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register_submit:
                registerUser();
                break;
            case R.id.iv_register_back:
                startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("Registration...");
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object obj) {
        progressDialog.cancel();

        if (obj instanceof EmptyResponse) {
            EmptyResponse result = (EmptyResponse) obj;
            if (result != null && result.isSuccess()) {

                int userId = result.getData().get(0).getId();
                String email = result.getData().get(0).getEmail();
                String firstName = result.getData().get(0).getFirst_name();
                String lastName = result.getData().get(0).getLast_name();

                SharedPreferenceUtils.getInstance(SignUpActivity.this).setUserId(userId);
                SharedPreferenceUtils.getInstance(SignUpActivity.this).setUserFirstName(firstName);
                SharedPreferenceUtils.getInstance(SignUpActivity.this).setUserLastName(lastName);
                SharedPreferenceUtils.getInstance(SignUpActivity.this).setUserEmail(email);

                SharedPreferenceUtils.getInstance(SignUpActivity.this).SetLoginCheck("success");
                Intent intent = new Intent(SignUpActivity.this, Dashboard.class);
                startActivity(intent);
            } else {
                Toast.makeText(SignUpActivity.this, "Signup Faild",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void doInBackground() {
    }
}
