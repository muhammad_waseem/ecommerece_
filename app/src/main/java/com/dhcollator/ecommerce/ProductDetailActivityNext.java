package com.dhcollator.ecommerce;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dhcollator.ecommerce.dtos.ProductsDTO;
import com.dhcollator.ecommerce.interfaces.OnItemClick;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetailActivityNext extends AppCompatActivity implements View.OnClickListener {

    TextView titleTV;
    TextView priceTV;
    TextView locationTV;
    TextView brandTV;
    TextView yearTV;
    TextView conditionTV;
    TextView contactTV;
    TextView detailTV;
    Button nextBTN;

    public ProductDetailActivityNext(){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        titleTV = (TextView) findViewById(R.id.tv_product_detail_title);
        priceTV = (TextView) findViewById(R.id.tv_product_detail_price);
        locationTV = (TextView) findViewById(R.id.tv_product_detail_location);
        brandTV = (TextView) findViewById(R.id.tv_product_detail_brand);
        yearTV = (TextView) findViewById(R.id.tv_product_detail_year);
        conditionTV = (TextView) findViewById(R.id.tv_product_detail_condition);
        contactTV = (TextView) findViewById(R.id.tv_product_detail_contact);
        detailTV = (TextView) findViewById(R.id.tv_product_detail_detail);
        nextBTN = (Button) findViewById(R.id.btn_detail_product_next);
        nextBTN.setOnClickListener(this);

        ProductsDTO detailData = (ProductsDTO) getIntent().getParcelableExtra("detail");

        titleTV.setText(detailData.getName());
        priceTV.setText(detailData.getPrice());
        locationTV.setText(detailData.getLocation());
        brandTV.setText(detailData.getBrand());
        yearTV.setText(detailData.getCreated_at());
        conditionTV.setText(detailData.getPresent_condition());
        contactTV.setText(detailData.getContact());
        detailTV.setText(detailData.getDetail());
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ProductDetailActivityNext.this, Dashboard.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_detail_product_next:
                Log.d("Clicked", "here");
                break;
        }
    }
}
