package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhcollator.ecommerce.adapters.ProductsAdapter;
import com.dhcollator.ecommerce.dtos.ProductsDTO;
import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.dhcollator.ecommerce.fragments.ProductsFragment;
import com.dhcollator.ecommerce.interfaces.OnCallback;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.ProductsEntityResponseModel;
import com.dhcollator.ecommerce.response.ProductsResponseModel;
import com.dhcollator.ecommerce.response.UserResponseModel;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;
import com.firebase.client.Firebase;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, INetworkListener, OnItemClick, Constants {


    RecyclerView productsRV;
    ProductsAdapter productsAdapter;
    ArrayList<ProductsDTO> data = new ArrayList<>();
    private ProgressDialog progressDialog;
    TextView headerNameTV;
    TextView headerEmailTV;
    Firebase ref;
    DisplayImageOptions options;
    ImageView profilePicIV;

    public Dashboard() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard2);
        Firebase.setAndroidContext(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(Dashboard.this);
        Firebase.setAndroidContext(this);
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(Dashboard.this));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(100))
                .build();

        productsRV = (RecyclerView) findViewById(R.id.rv_dashboard_products);
        LinearLayoutManager llm = new LinearLayoutManager(Dashboard.this);
        productsRV.setLayoutManager(llm);
        NetworkOperations.getInstance().getAllProducts(Dashboard.this, this);
        productsAdapter = new ProductsAdapter(data, this,Dashboard.this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        String firstName = SharedPreferenceUtils.getInstance(Dashboard.this).getUserFirstName();
        String lastName = SharedPreferenceUtils.getInstance(Dashboard.this).getUserLastName();
        String userEmail = SharedPreferenceUtils.getInstance(Dashboard.this).getUuserEmail();
        View headerView = LayoutInflater.from(this).inflate(R.layout.nav_header_dashboard, navigationView, false);

        navigationView.addHeaderView(headerView);

        int loggedInUserId = SharedPreferenceUtils.getInstance(Dashboard.this).getUserId();
        headerNameTV = (TextView) headerView.findViewById(R.id.tv_dashboard_header_name);
        headerEmailTV = (TextView) headerView.findViewById(R.id.tv_dashboard_header_email);
        profilePicIV=(ImageView)headerView.findViewById(R.id.iv_dashboard_header_pic);
        headerNameTV.setText(firstName);
        headerEmailTV.setText(userEmail);

        String keyDp = loggedInUserId + "_dp";
        ImageLoader.getInstance().displayImage(S3_BASE_URL + keyDp, profilePicIV, options);

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//       // getMenuInflater().inflate(R.menu.dashboard, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_drawer_Dashboard) {
            Intent intent = new Intent(Dashboard.this, Dashboard.class);
            startActivity(intent);
        } else if (id == R.id.nav_drawer_inbox) {
            Intent intent = new Intent(Dashboard.this, InboxActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_drawer_profile) {
            Intent intent = new Intent(Dashboard.this, ProfileActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_drawer_post) {
            Intent intent = new Intent(Dashboard.this, AddPostActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_drawer_adds) {
            Intent intent = new Intent(Dashboard.this, MyAddsActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_drawer_setting) {
            Intent intent = new Intent(Dashboard.this, SettingActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_drawer_logout) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard.this);
            alertDialogBuilder.setMessage(R.string.logout_confirmation);
            alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {

                    SharedPreferenceUtils.getInstance(Dashboard.this).clearCache();
                    Intent splash = new Intent(Dashboard.this, MainActivity.class);
                    startActivity(splash);
                    finish();

                }
            });
            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("Loading data ...");
        progressDialog.show();

    }

    @Override
    public void onPostExecute(Object obj) {

        progressDialog.cancel();
        if (obj instanceof ProductsResponseModel) {
            ProductsResponseModel result = (ProductsResponseModel) obj;
            if (result.isSuccess()) {
                Log.d("Success", "" + result.isSuccess());
                for (int i = 0; i < result.getProducts().size(); i++) {
                    ProductsEntityResponseModel currentEntity = result.getProducts().get(i);
                    ProductsDTO productsDTO = new ProductsDTO();
                    productsDTO.setId(currentEntity.getId());
                    productsDTO.setName(currentEntity.getName());
                    productsDTO.setCode(currentEntity.getCode());
                    productsDTO.setCreated_at(currentEntity.getCreated_at());
                    productsDTO.setStatus(currentEntity.getStatus());
                    productsDTO.setContact(currentEntity.getContact());
                    productsDTO.setBrand(currentEntity.getBrand());
                    productsDTO.setLocation(currentEntity.getLocation());
                    productsDTO.setDetail(currentEntity.getDetail());
                    productsDTO.setPrice(currentEntity.getPrice());
                    productsDTO.setCreated_at(currentEntity.getCreated_at());
                    productsDTO.setUser_id(currentEntity.getUser_id());
                    productsDTO.setPresent_condition(currentEntity.getPresent_condition());
                    data.add(productsDTO);
                    productsRV.setAdapter(productsAdapter);

                }
            }
        }
        if (obj instanceof UserResponseModel) {
            UserResponseModel result = (UserResponseModel) obj;
            if (result.isSuccess()) {
                UserDTO1 userDetail = new UserDTO1();
                userDetail.setFirstName(result.getData().get(0).getFirstName());
                userDetail.setLastName(result.getData().get(0).getLastName());
                userDetail.setEmail(result.getData().get(0).getEmail());
                userDetail.setId(result.getData().get(0).getId());
                Intent intent = new Intent(Dashboard.this, NewChatActivity.class);
                intent.putExtra("detail", userDetail);
                startActivity(intent);
            }

        }


    }

    @Override
    public void doInBackground() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onClick(int pPosition, int id) {
        ProductsDTO detailData = data.get(pPosition);
//        if (id == 100) {
//            Intent intent = new Intent(Dashboard.this, ProductDetailActivityNext.class);
//            intent.putExtra("detail", data);
//            startActivity(intent);
//           finish();
//        }
        if (id == 100) {
            Intent intent = new Intent(Dashboard.this, ProductDetailActivity.class);
            intent.putExtra("detail", detailData);
            startActivity(intent);
            finish();
        }

        if (id == 101) {
            String action = Constants.ACTION_GET_MY_PROFILE + "/" + detailData.getUser_id();
            NetworkOperations.getInstance().getData(Dashboard.this, action, null, this, UserResponseModel.class);
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
