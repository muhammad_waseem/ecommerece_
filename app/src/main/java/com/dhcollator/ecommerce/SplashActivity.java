package com.dhcollator.ecommerce;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new MyTimer(2000, 2000).start();
    }

    private final class MyTimer extends CountDownTimer {

        public MyTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
           int isLogin = SharedPreferenceUtils.getInstance(SplashActivity.this).getUserId();
            Log.d("LOGIN-ID",""+isLogin);
            if (isLogin !=-1 ) {
                startActivity(new Intent(SplashActivity.this, Dashboard.class));
                finish();
            }else {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}
