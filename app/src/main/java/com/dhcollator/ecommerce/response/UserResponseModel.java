package com.dhcollator.ecommerce.response;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Lenovo on 6/21/2016.
 */
public class UserResponseModel implements Serializable {

    boolean success;
    ArrayList<UserEntityResponseModel>users;
    ArrayList<UserEntityResponseModel>data;

    public ArrayList<UserEntityResponseModel> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserEntityResponseModel> users) {
        this.users = users;
    }

    public ArrayList<UserEntityResponseModel> getData() {
        return data;
    }

    public void setData(ArrayList<UserEntityResponseModel> data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public UserResponseModel(){

    }
}
