package com.dhcollator.ecommerce.response;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Lenovo on 8/12/2016.
 */
public class AddProductResponse implements Serializable {


    String message;
    boolean success;
    ArrayList<AddProductEntityResponse> data;

    public AddProductResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public ArrayList<AddProductEntityResponse> getData() {
        return data;
    }

    public void setData(ArrayList<AddProductEntityResponse> data) {
        this.data = data;
    }

}
