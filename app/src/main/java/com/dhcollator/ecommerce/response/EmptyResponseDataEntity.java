package com.dhcollator.ecommerce.response;

import java.io.Serializable;

/**
 * Created by Lenovo on 6/28/2016.
 */
public class EmptyResponseDataEntity implements Serializable {

    int id;
    String email;
    String last_name;
    String first_name;

    public EmptyResponseDataEntity() {


    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
