package com.dhcollator.ecommerce.response;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Lenovo on 6/28/2016.
 */
public class EmptyResponse implements Serializable {

    String message;
    boolean success;
    ArrayList<EmptyResponseDataEntity> data;

    public ArrayList<EmptyResponseDataEntity> getData() {
        return data;
    }

    public void setData(ArrayList<EmptyResponseDataEntity> data) {
        this.data = data;
    }

    public EmptyResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
