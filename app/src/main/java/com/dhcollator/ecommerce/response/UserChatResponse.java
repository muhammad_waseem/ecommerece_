package com.dhcollator.ecommerce.response;

import android.util.Log;
import android.widget.Toast;

import java.io.Serializable;
import java.sql.Timestamp;

import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by Lenovo on 7/16/2016.
 */
public class UserChatResponse implements Serializable, Comparable<UserChatResponse> {

    String lastMessage;
    String lastMessageSender;
    String chat_id;
    String text;
    String senderName;
    Long messageTime;
    Timestamp messageTime1;
    private String randomId;
    String lastMessageSenderName;

    public UserChatResponse() {

    }
    UserChatResponse(String text, Timestamp timestamp1, String sender) {
        this.text = text;
        this.messageTime1 = timestamp1;
        this.senderName = sender;

    }

    public String getLastMessageSenderName() {
        return lastMessageSenderName;
    }

    public void setLastMessageSenderName(String lastMessageSenderName) {
        this.lastMessageSenderName = lastMessageSenderName;
    }

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Long messageTime) {
        this.messageTime = messageTime;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageSender() {
        return lastMessageSender;
    }

    public void setLastMessageSender(String lastMessageSender) {
        this.lastMessageSender = lastMessageSender;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    @Override
    public int hashCode() {
        return messageTime.hashCode();
    }


    @Override
    public boolean equals(Object o) {
        UserChatResponse chatMessage = (UserChatResponse) o;
        if (o instanceof UserChatResponse) {
            if (chatMessage.getMessageTime() != null
                    && chatMessage.getMessageTime()
                    .equals(this.getMessageTime())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public int compareTo(UserChatResponse another) {
        Timestamp timestamp = new Timestamp(another.getMessageTime());
        Timestamp timestamp2=this.messageTime1;

        Log.d("TimpSpan1",""+timestamp);
        Log.d("TimpSpan2",""+timestamp2);


        if (timestamp.before(timestamp2)){
            return 1;
        }
        else{
            return -1;
        }
    }
}
