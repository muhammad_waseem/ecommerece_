package com.dhcollator.ecommerce.response;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Lenovo on 6/28/2016.
 */
public class MyAddsResponse
        implements Serializable {

    String message;
    boolean success;
    ArrayList<MyAddsDataEntity> data;

    public MyAddsResponse() {
    }
    public ArrayList<MyAddsDataEntity> getData() {
        return data;
    }

    public void setData(ArrayList<MyAddsDataEntity> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}