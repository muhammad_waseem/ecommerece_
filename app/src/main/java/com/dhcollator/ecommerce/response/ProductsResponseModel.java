package com.dhcollator.ecommerce.response;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Lenovo on 6/25/2016.
 */
public class ProductsResponseModel implements Serializable {

    boolean success;
    ArrayList<ProductsEntityResponseModel> products;
    public ProductsResponseModel(){

    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }


    public ArrayList<ProductsEntityResponseModel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ProductsEntityResponseModel> products) {
        this.products = products;
    }

}
