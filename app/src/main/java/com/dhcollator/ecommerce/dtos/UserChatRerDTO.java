package com.dhcollator.ecommerce.dtos;

import java.io.Serializable;

/**
 * Created by Lenovo on 7/16/2016.
 */
public class UserChatRerDTO implements Serializable {

    String lastMessage;
    String lastMessageSender;
    String chat_id;
    String text;
    String senderName;
    Long messageTime;
    private String randomId;

    UserChatRerDTO() {

    }

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Long messageTime) {
        this.messageTime = messageTime;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageSender() {
        return lastMessageSender;
    }

    public void setLastMessageSender(String lastMessageSender) {
        this.lastMessageSender = lastMessageSender;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    @Override
    public int hashCode(){
        return messageTime.hashCode();
    }


    @Override
    public boolean equals(Object o) {
        UserChatRerDTO chatMessage = (UserChatRerDTO) o;
        if (o instanceof UserChatRerDTO) {
            if (chatMessage.getMessageTime()!= null
                    && chatMessage.getMessageTime()
                    .equals(this.getMessageTime())) {
                return true;
            }
        }
        return false;
    }
}
