package com.dhcollator.ecommerce.dtos;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Lenovo on 6/25/2016.
 */
public class ProductsDTO implements Parcelable{

    int id;
    String detail;
    String contact;
    String present_condition;
    String 	brand;
    String location;
    String price;
    String name;
    String code;
    String status;
    int user_id;
    String created_at;

    public ProductsDTO() {

    }

    protected ProductsDTO(Parcel in) {
        id = in.readInt();
        detail = in.readString();
        contact = in.readString();
        present_condition = in.readString();
        brand = in.readString();
        location = in.readString();
        price = in.readString();
        name = in.readString();
        code = in.readString();
        status = in.readString();
        user_id = in.readInt();
        created_at = in.readString();
    }

    public static final Creator<ProductsDTO> CREATOR = new Creator<ProductsDTO>() {
        @Override
        public ProductsDTO createFromParcel(Parcel in) {
            return new ProductsDTO(in);
        }

        @Override
        public ProductsDTO[] newArray(int size) {
            return new ProductsDTO[size];
        }
    };

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getPresent_condition() {
        return present_condition;
    }

    public void setPresent_condition(String present_condition) {
        this.present_condition = present_condition;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(detail);
        dest.writeString(contact);
        dest.writeString(present_condition);
        dest.writeString(brand);
        dest.writeString(location);
        dest.writeString(price);
        dest.writeString(name);
        dest.writeString(code);
        dest.writeString(status);
        dest.writeInt(user_id);
        dest.writeString(created_at);
    }
}
