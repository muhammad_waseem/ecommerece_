package com.dhcollator.ecommerce;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mLogoIV;
    private Intent intent;
    Button mLoginMainBTN;
    Button mRegisterBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Main");

        mLogoIV=(ImageView)findViewById(R.id.logo);
        mLoginMainBTN=(Button) findViewById(R.id.btn_login);
        mRegisterBTN=(Button) findViewById(R.id.btn_register);

        mLoginMainBTN.setOnClickListener(this);
        mRegisterBTN.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_register:
                intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
