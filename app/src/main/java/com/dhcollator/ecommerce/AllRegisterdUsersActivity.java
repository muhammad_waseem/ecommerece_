package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.dhcollator.ecommerce.adapters.UsersAdapter;
import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.dhcollator.ecommerce.models.RegisterResponseModel;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.UserEntityResponseModel;
import com.dhcollator.ecommerce.response.UserResponseModel;

import java.util.ArrayList;
import java.util.List;

public class AllRegisterdUsersActivity extends AppCompatActivity implements INetworkListener {

    UsersAdapter mUserAdapter;
    private RecyclerView mRV;
    List<UserDTO1> mUsersItems = new ArrayList<>();
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);
        setTitle("Dashboard");
        mRV = (RecyclerView) findViewById(R.id.rv_all_users);
        NetworkOperations.getInstance().getAllUsers(this, this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mUserAdapter = new UsersAdapter(mUsersItems);
        mRV.setLayoutManager(llm);


    }

    @Override
    public void onPreExecute() {
        progress = new ProgressDialog(this);
        progress.setMessage("Fetching data.....");
        progress.show();
    }

    @Override
    public void onPostExecute(Object obj) {
        progress.dismiss();
        if (obj instanceof RegisterResponseModel) {
            RegisterResponseModel result = (RegisterResponseModel) obj;
            String id = result.getId();
            String var = result.getLastName();
            String var1 = result.getFirstName();
            String var2 = result.getPassword();
            String var3 = result.getEmail();
            UserDTO1 obj1 = new UserDTO1();
            obj1.setFirstName(var1);
            obj1.setLastName(var);
            obj1.setEmail(var3);
            obj1.setPassword(var2);
            obj1.setId(id);
            mUsersItems.add(obj1);
            mRV.setAdapter(mUserAdapter);
        } else if (obj instanceof UserResponseModel) {

            UserResponseModel result = (UserResponseModel) obj;
            Log.d("Success", "" + result.isSuccess());
            for (int i = 0; i < result.getUsers().size(); i++) {
                UserEntityResponseModel currentEntity = result.getUsers().get(i);
                String f1 = currentEntity.getFirstName();
                String f2 = currentEntity.getLastName();
                String f3 = currentEntity.getEmail();
                String f0=currentEntity.getId();
                String f4 = currentEntity.getPassword();
                UserDTO1 obj1 = new UserDTO1();
                obj1.setFirstName(f1);
                obj1.setLastName(f2);
                obj1.setEmail(f3);
                obj1.setId(f0);
                obj1.setPassword(f4);
                mUsersItems.add(obj1);
                mRV.setAdapter(mUserAdapter);
                mUserAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void doInBackground() {

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AllRegisterdUsersActivity.this, Dashboard.class);
        startActivity(intent);
        finish();
    }
}
