package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.EditText;

import com.dhcollator.ecommerce.adapters.MyAddsAdapter;
import com.dhcollator.ecommerce.dtos.MyAddsDTO;
import com.dhcollator.ecommerce.dtos.ProductsDTO;
import com.dhcollator.ecommerce.interfaces.OnCallback;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.models.BaseModel;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.MyAddsDataEntity;
import com.dhcollator.ecommerce.response.MyAddsResponse;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyAddsActivity extends AppCompatActivity implements View.OnClickListener, INetworkListener, OnItemClick {

    private RecyclerView mMyAddsRV;
    MyAddsAdapter myAddsAdapter;
    List<MyAddsDTO> data = new ArrayList<>();
    private ProgressDialog progressDialog;
    int rowId;
    int rowDeletePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_adds);

        setTitle("My Ads");
        mMyAddsRV = (RecyclerView) findViewById(R.id.rv_my_adds);
        progressDialog = new ProgressDialog(MyAddsActivity.this);
        LinearLayoutManager llm = new LinearLayoutManager(MyAddsActivity.this);
        mMyAddsRV.setLayoutManager(llm);
        myAddsAdapter = new MyAddsAdapter(data, this,MyAddsActivity.this);

        int userId = SharedPreferenceUtils.getInstance(MyAddsActivity.this).getUserId();
        String action = Constants.ACTION_GET_MY_ADDS + "/" + userId;
        NetworkOperations.getInstance().getData(MyAddsActivity.this, action, null, this, MyAddsResponse.class);

    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("Deleting data...");
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object obj) {
        progressDialog.cancel();
        if (obj instanceof MyAddsResponse) {
            MyAddsResponse result = (MyAddsResponse) obj;
            if (result.isSuccess()) {
                for (int i = 0; i < result.getData().size(); i++) {
                    MyAddsDataEntity currentEntity = result.getData().get(i);
                    MyAddsDTO myAddsObj = new MyAddsDTO();
                    myAddsObj.setName(currentEntity.getName());
                    myAddsObj.setStatus(currentEntity.getStatus());
                    myAddsObj.setCode(currentEntity.getCode());
                    myAddsObj.setPrice(currentEntity.getPrice());
                    myAddsObj.setStatus(currentEntity.getStatus());
                    myAddsObj.setBrand(currentEntity.getBrand());
                    myAddsObj.setContact(currentEntity.getContact());
                    myAddsObj.setDetail(currentEntity.getContact());
                    myAddsObj.setCreated_at(currentEntity.getCreated_at());
                    myAddsObj.setId(currentEntity.getId());
                    myAddsObj.setUser_id(currentEntity.getUser_id());
                    myAddsObj.setLocation(currentEntity.getLocation());
                    data.add(myAddsObj);
                    mMyAddsRV.setAdapter(myAddsAdapter);
                }
            }
        }
        if (obj instanceof BaseModel){
            BaseModel result=(BaseModel)obj;
            if (result.isSuccess()){
                data.remove(rowDeletePosition);
                myAddsAdapter.notifyDataSetChanged();
            }

        }

    }

    @Override
    public void doInBackground() {

    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void onClick(int pPosition, int id) {

        final MyAddsDTO detailData = data.get(pPosition);
        switch (id) {
            case 101:

                Intent intent = new Intent(MyAddsActivity.this, AddPostActivity.class);
                intent.putExtra("editData", detailData);
                intent.putExtra("edit", true);
                startActivity(intent);


                break;
            case 102:
                 rowId = detailData.getId();
                 rowDeletePosition=pPosition;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyAddsActivity.this);
                alertDialogBuilder.setMessage("are you sure you want to delete this");
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                       deleteRow();
                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }

    }
    public void deleteRow(){
        String action = Constants.ACTION_DELETE_ROW + "/" + rowId;
        NetworkOperations.getInstance().deleteData1(MyAddsActivity.this, action, null, this, BaseModel.class);
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MyAddsActivity.this, Dashboard.class);
        startActivity(intent);
        finish();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
