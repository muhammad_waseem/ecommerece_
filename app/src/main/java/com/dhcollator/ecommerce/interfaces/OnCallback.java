package com.dhcollator.ecommerce.interfaces;

/**
 * Created by waleed on 7/9/15.
 */
public interface OnCallback {

    public void onClickCallback();
    public void onItemClick(int position);
}
