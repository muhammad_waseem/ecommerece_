package com.dhcollator.ecommerce.interfaces;

/**
 * Created by Talha.Hafeez on 12/16/2015.
 */
public interface OnItemClick {

    public void onClick(int pPosition, int id);


}
