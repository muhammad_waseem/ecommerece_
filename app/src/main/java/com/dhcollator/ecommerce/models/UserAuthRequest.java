package com.dhcollator.ecommerce.models;

/**
 * Created by Lenovo on 5/26/2016.
 */
public class UserAuthRequest {

    String user;
    String password;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
