package com.dhcollator.ecommerce.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lenovo on 5/26/2016.
 */
public class RegisterUser {

    public static class Request {
        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private int age;

         public Request(String email, String password, String firstName, String lastName, int age) {
             this.email = email;
             this.password = password;
             this.firstName = firstName;
             this.lastName = lastName;
             this.age = age;
         }
        public JSONObject getRequest() {
            JSONObject obj = new JSONObject();
            JSONObject params = new JSONObject();
            try {
                params.put("email", email);
                params.put("password", password);
                params.put("first_name", firstName);
                params.put("last_name", lastName);
                params.put("age", age);
                obj.put("user", params);
               // Log.d("Test", obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }
    }
}
