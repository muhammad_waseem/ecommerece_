package com.dhcollator.ecommerce;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ToggleButton;

import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingActivity extends Activity {

    ToggleButton notificationTB;
    ToggleButton soundTB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        notificationTB = (ToggleButton) findViewById(R.id.tb_settings_notifications);
        soundTB = (ToggleButton) findViewById(R.id.tb_settings_sound);

        notificationTB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Log.d("Notification","TRUE");
                }
                else
                {
                    Log.d("Notification","False");
                }
            }
        });

        soundTB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Log.d("Sound","TRUE");
                }
                else
                {
                    Log.d("Sound","False");
                }
            }
        });

    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(SettingActivity.this, Dashboard.class));
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
