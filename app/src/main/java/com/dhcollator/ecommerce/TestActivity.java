package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dhcollator.ecommerce.dtos.request.AddPostRequest;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.EmptyResponse;
import com.dhcollator.ecommerce.utils.Constants;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class TestActivity extends AppCompatActivity implements View.OnClickListener, INetworkListener {
    EditText titleET;
    EditText priceET;
    ProgressDialog progressDialog;
    ImageView saveAddIV;
    ImageView capturePic;
    private static int RESULT_LOAD_GALLERY = 1;
    private static int RESULT_LOAD_CAMERA = 0;
    MultipartEntityBuilder entity = MultipartEntityBuilder.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        titleET = (EditText) findViewById(R.id.et_add_post_title);
        priceET = (EditText) findViewById(R.id.et_add_post_price);
        saveAddIV = (ImageView) findViewById(R.id.iv_add_post_save);
        capturePic = (ImageView) findViewById(R.id.iv_select_pic);
        capturePic.setOnClickListener(this);
        saveAddIV.setOnClickListener(this);
        progressDialog = new ProgressDialog(TestActivity.this);

        findViewById(R.id.iv_add_post_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
        findViewById(R.id.iv_select_pic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_GALLERY);
            }
        });

    }


    public void saveData() {

        String action = Constants.ACTION_POST_ADDS_PRODUCTS;


        AddPostRequest request = new AddPostRequest();
        String name = titleET.getText().toString();
        String price = priceET.getText().toString();

        request.setName(name);
        request.setCode(price);
        request.setStatus("active");

        NetworkOperations.getInstance().postData(TestActivity.this, action, request, this, EmptyResponse.class);

    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("posting...");
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object obj) {

        progressDialog.cancel();
        if (obj instanceof EmptyResponse) {
            EmptyResponse result = (EmptyResponse) obj;
            if (result.isSuccess()) {
                Toast.makeText(TestActivity.this, result.getMessage(),
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(TestActivity.this, "failed to post product",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void doInBackground() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_CAMERA && resultCode == RESULT_OK && null != data) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            String stringImg = getStringImage(photo);

            FileBody fileBody = new FileBody(new File(stringImg));
            entity.addPart("picture", fileBody);

            capturePic.setImageBitmap(photo);

        }

        if (requestCode == RESULT_LOAD_GALLERY && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            FileBody fileBody = new FileBody(new File(picturePath));
            entity.addPart("picture", fileBody);


            capturePic.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }

    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
