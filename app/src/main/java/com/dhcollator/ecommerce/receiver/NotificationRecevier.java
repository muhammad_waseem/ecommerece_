package com.dhcollator.ecommerce.receiver;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dhcollator.ecommerce.MainActivity;
import com.dhcollator.ecommerce.NewChatActivity;
import com.dhcollator.ecommerce.R;
import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lenovo on 8/12/2016.
 */
public class NotificationRecevier extends BroadcastReceiver {

    Intent myintent;
    Context context;
    UserDTO1 newObj;
    Intent resultIntent;
    TaskStackBuilder stackBuilder;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.myintent = intent;
        this.context = context;

        Bundle extras = intent.getExtras();
        String message = (String) extras.get("message");

        String jsonString = intent.getStringExtra("map");
        showNotification(context, jsonString, message);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void showNotification(Context mContext, String json, String message) {
        try {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
            mBuilder.setContentTitle("New Message");
            mBuilder.setContentText(message);
            mBuilder.setAutoCancel(true);
            mBuilder.setTicker("New Message ");
            mBuilder.setSound(uri);
            mBuilder.setSmallIcon(R.drawable.icon_peddlemi);

            try {
                JSONObject obj = new JSONObject(json);
                String lastName = obj.getString("last_name");
                String id = obj.getString("id");
                String firstName = obj.getString("first_name");
                String email = obj.getString("email");
                newObj = new UserDTO1();
                newObj.setId(id);
                newObj.setFirstName(firstName);
                newObj.setLastName(lastName);
                newObj.setEmail(email);

                resultIntent = new Intent(mContext, NewChatActivity.class);
                resultIntent.putExtra("detail", newObj);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            stackBuilder = TaskStackBuilder.create(mContext);
            stackBuilder.addParentStack(MainActivity.class);
           /* Adds the Intent that starts the Activity to the top of the stack */
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(101, PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    	   /* notificationID allows you to update the notification later on. */
            mNotificationManager.notify(1001, mBuilder.build());

        } finally {

        }
    }

}
