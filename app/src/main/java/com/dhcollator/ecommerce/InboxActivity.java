package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.dhcollator.ecommerce.adapters.InboxAdapter;
import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.MyRoomsResponse;
import com.dhcollator.ecommerce.response.UserChatResponse;
import com.dhcollator.ecommerce.response.UserResponseModel;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InboxActivity extends AppCompatActivity implements Constants,OnItemClick,INetworkListener {

    Firebase getAllChatId;
    RecyclerView inboxRV;
    InboxAdapter inboxAdapter;
    private ProgressDialog progressDialog;
    private ArrayList<MyRoomsResponse> myAllRoomsId = new ArrayList<MyRoomsResponse>();
    private ArrayList<UserChatResponse> data = new ArrayList<UserChatResponse>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        setTitle("Inbox");
        Firebase.setAndroidContext(this);
        progressDialog = new ProgressDialog(InboxActivity.this);
        inboxRV = (RecyclerView) findViewById(R.id.rv_inbox);
        LinearLayoutManager llm = new LinearLayoutManager(InboxActivity.this);
        inboxRV.setLayoutManager(llm);

        int loggedInUserId = SharedPreferenceUtils.getInstance(InboxActivity.this).getUserId();
        getAllChatId = new Firebase(FIREBASE_URL + "/" + MYROOMS + "/" + loggedInUserId);
        Query getAllChatRooms = getAllChatId;
        getAllChatRooms.addListenerForSingleValueEvent(listener);
        inboxAdapter = new InboxAdapter(data,this,InboxActivity.this);
        inboxRV.setAdapter(inboxAdapter);
    }

    private ValueEventListener listener = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot snap) {

            if (snap != null) {
                Object value = snap.getValue();
                @SuppressWarnings("rawtypes")
                final Map map = (Map) value;
                if (map != null) {
                    for (Object key : map.keySet()) {
                        @SuppressWarnings("unchecked")
                        Map<Object, Object> obj = (Map<Object, Object>) map.get(key);
                        JSONObject o = new JSONObject(obj);
                        Gson gson = new Gson();
                        MyRoomsResponse chatUser = gson.fromJson(o.toString(), MyRoomsResponse.class);
                        myAllRoomsId.add(0,chatUser);
                        Log.d("ArraySizeRooms", ":" + myAllRoomsId.size());
                    }
                    roomInFo();
                }
                Log.d("Map", "Null");

            } else {
                Log.d("SnapShot", "Null");
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    };

    public void roomInFo() {
        for (int i = 0; i < myAllRoomsId.size(); i++) {
            String chatRoomId = myAllRoomsId.get(i).getChat_id();
            Log.d("ChatRoomId", "" + chatRoomId);

            Firebase ref = new Firebase(FIREBASE_URL + "/" + CHATROOMS + "/" + chatRoomId + "/" + INFO);
            Query getRoomInfo = ref;
            getRoomInfo.addListenerForSingleValueEvent(roomInfoListner);

        }
    }

    private ValueEventListener roomInfoListner = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            Log.d("SNAP-CHATROOM", "" + dataSnapshot.getValue());

            if (dataSnapshot != null) {
                try {
                    @SuppressWarnings("rawtypes")
                    final Map map = (Map) dataSnapshot.getValue();
                    if (map != null) {
                        JSONObject o = new JSONObject(map);
                        Log.d("Inbox-fetching", "" + o.toString());
                        Gson gson = new Gson();
                        UserChatResponse chatUser = gson.fromJson(o.toString(), UserChatResponse.class);
                        Log.d("Inbox-LstMessage", "" + chatUser.getLastMessage());
                        Log.d("Inbox-Name", "" + chatUser.getSenderName());
                        data.add(chatUser);
                        inboxAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Log.e("Chat", e.getMessage());
                }
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    };


    @Override
    public void onBackPressed() {
        startActivity(new Intent(InboxActivity.this, Dashboard.class));
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(int pPosition, int id) {
        UserChatResponse getId = data.get(pPosition);
        if (id==100) {

            String chatId=getId.getChat_id();
            String[] splitChatId=chatId.split("-");


            int firstPersonId= Integer.parseInt(splitChatId[0]);
            int secondPersonId= Integer.parseInt(splitChatId[1]);

            int loggedInUserId = SharedPreferenceUtils.getInstance(InboxActivity.this).getUserId();
            if (firstPersonId!=loggedInUserId){

                String action = Constants.ACTION_GET_MY_PROFILE + "/" + firstPersonId;
                NetworkOperations.getInstance().getData(InboxActivity.this, action, null, this, UserResponseModel.class);
            }else {
                String action = Constants.ACTION_GET_MY_PROFILE + "/" + secondPersonId;
                NetworkOperations.getInstance().getData(InboxActivity.this, action, null, this, UserResponseModel.class);
            }


        }
    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("Loading data ...");
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object obj) {
        progressDialog.cancel();

        if (obj instanceof UserResponseModel) {
            UserResponseModel result = (UserResponseModel) obj;
            if (result.isSuccess()) {
                UserDTO1 userDetail = new UserDTO1();
                userDetail.setFirstName(result.getData().get(0).getFirstName());
                userDetail.setLastName(result.getData().get(0).getLastName());
                userDetail.setEmail(result.getData().get(0).getEmail());
                userDetail.setId(result.getData().get(0).getId());
                Intent intent = new Intent(InboxActivity.this, NewChatActivity.class);
                intent.putExtra("detail", userDetail);
                UserDTO1 userDetail1 = new UserDTO1();
               startActivity(intent);
            }

        }

    }

    @Override
    public void doInBackground() {

    }
}
