package com.dhcollator.ecommerce.utils;

/**
 * Created by Lenovo on 5/26/2016.
 */
public interface Constants {

    public static final String URL_BASE = "http://192.168.3.104/Ecommerce/public/";

    //  public static final String URL_BASE = "http://2b70ac6a.ngrok.io/Ecommerce/public/";
    public static final String FIREBASE_URL = "https://peddlemi-b9298.firebaseio.com";
    public static final String S3_BASE_URL = "https://s3.amazonaws.com/paddlemi/";
    public static final String BROADCAST = "com.dhcollator.ecommerce.android.action.broadcast";

    public static final String ACTION_REGISTERATION = "register";
    public static final String ACTION_AUTH_USER = "login";
    public static final String ACTION_ALL_USERS = "allUsers";
    public static final String ACTION_ALL_PRODUCTS = "getAllProducts";
    public static final String ACTION_POST_ADDS_PRODUCTS = "addProduct";
    public static final String ACTION_GET_MY_ADDS = "getMyAdds";
    public static final String ACTION_GET_MY_PROFILE = "myProfile";
    public static final String ACTION_UPDATE_PRODUCT = "updateProduct";
    public static final String ACTION_DELETE_ROW = "deleteProduct";


    public static String PREFS_NAME = "app_prefs";
    public static String PREFS_LOGIN = "login";
    public static String PREFS_USER_ID = "UserId";
    public static String PREFS_USER_FIRST_NAME = "fName";
    public static String PREFS_USER_LAST_NAME = "lName";
    public static String PREFS_USER_EMAIL = "email";
    public static String USERS = "Users";
    public static String MYROOMS = "MyRooms";
    public static String CHATROOMS = "ChatRooms";
    public static String INFO = "info";
    public static String MEMBERS = "members";
    public static String MESSAGES = "messages";
    public static final String S3_BUCKET_NAME = "paddlemi";
    public static final String S3_ACCESS_KEY_ID = "AKIAIKZCPZA27TU42GGQ";
    public static final String S3_SECRET_KEY = "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";
    public static final String S3_POOL_ID = "us-east-1:4f4418ee-ce61-43b7-8f04-3c641d45da2a";

}
