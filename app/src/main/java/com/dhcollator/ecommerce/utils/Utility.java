package com.dhcollator.ecommerce.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utility {

    private static Utility mInstance;

    private Utility() {
    }

    public static Utility getInstance() {
        if (mInstance == null)
            mInstance = new Utility();
        return mInstance;
    }

    public static String getImageName(String mail) {
        return mail.replace("@", "_").replace(".", "_") + "_1.jpg";
    }
}
