package com.dhcollator.ecommerce.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;

/**
 * Created by Lenovo on 6/26/2016.
 */
public class SharedPreferenceUtils implements Constants {

    private SharedPreferences sharedPreferences;
    private static SharedPreferenceUtils utility;

    private SharedPreferenceUtils(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = (context).getSharedPreferences(
                    PREFS_NAME, Context.MODE_PRIVATE);
        }
    }

    public static synchronized SharedPreferenceUtils getInstance(Context c) {
        if (utility == null) {
            utility = new SharedPreferenceUtils(c);
        }
        return utility;
    }
    public void SetLoginCheck(String login) {
        sharedPreferences.edit().putString(PREFS_LOGIN, login).commit();
    }
    public void setUserFirstName(String userFirstName) {
        sharedPreferences.edit().putString(PREFS_USER_FIRST_NAME, userFirstName).commit();
    }
    public void setUserLastName(String userLastName) {
        sharedPreferences.edit().putString(PREFS_USER_LAST_NAME, userLastName).commit();
    }
    public void setUserEmail(String userEmail) {
        sharedPreferences.edit().putString(PREFS_USER_EMAIL, userEmail).commit();
    }

    public void setUserId(int userid) {
        sharedPreferences.edit().putInt(PREFS_USER_ID, userid).commit();
    }
    public int getUserId() {
          return sharedPreferences.getInt(PREFS_USER_ID,-1);
    }
    public String getUserFirstName() {
          return sharedPreferences.getString(PREFS_USER_FIRST_NAME,null);
    }
    public String getUserLastName() {
         return sharedPreferences.getString(PREFS_USER_LAST_NAME,null);
    }
    public String getUuserEmail() {
         return sharedPreferences.getString(PREFS_USER_EMAIL,null);

    }
    public String getLoginCheck() {
        return sharedPreferences.getString(PREFS_LOGIN, null);
    }
    public void clearCache() {
        sharedPreferences.edit().remove(PREFS_LOGIN).commit();
        sharedPreferences.edit().remove(PREFS_USER_ID).commit();
        sharedPreferences.edit().remove(PREFS_USER_FIRST_NAME).commit();
        sharedPreferences.edit().remove(PREFS_USER_LAST_NAME).commit();
        sharedPreferences.edit().remove(PREFS_USER_EMAIL).commit();
    }
}
