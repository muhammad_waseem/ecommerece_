package com.dhcollator.ecommerce;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;

import java.util.Set;

public class Settings extends AppCompatActivity implements View.OnClickListener {

    TextView logoutTV;

    public Settings() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        logoutTV = (TextView)findViewById(R.id.tv_settings_logout);
        logoutTV.setOnClickListener(this);
        Settings.this.setTitle("Settings");
    }

    @Override
    public void onClick(View v) {
    }
}
