package com.dhcollator.ecommerce.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhcollator.ecommerce.R;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.response.UserChatResponse;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;
import com.dhcollator.ecommerce.viewholder.InboxViewHolder;
import com.dhcollator.ecommerce.viewholder.ProductsViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by Lenovo on 8/10/2016.
 */
public class InboxAdapter extends RecyclerView.Adapter<InboxViewHolder> implements Constants {

    List<UserChatResponse> data;
    OnItemClick onItemClick;
    Context context;
    DisplayImageOptions options;

    public InboxAdapter(List<UserChatResponse> data, OnItemClick onItemClick, Context context) {
        this.data = data;
        this.onItemClick = onItemClick;
        this.context = context;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(100))
                .build();
    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inbox, parent, false);
        return new InboxViewHolder(v);
    }

    @Override
    public void onBindViewHolder(InboxViewHolder holder, final int position) {

        UserChatResponse item = data.get(position);
        String userId = item.getLastMessageSender();
        String keyDp = userId + "_dp";
        Log.d("URL", "" + S3_BASE_URL + keyDp);
        String loggedInUserFirstName = SharedPreferenceUtils.getInstance(context).getUserFirstName();

        ImageLoader.getInstance().displayImage(S3_BASE_URL + keyDp, holder.getProfilePicIV(), options);

        if (item.getLastMessageSenderName().equalsIgnoreCase(loggedInUserFirstName)) {
            holder.getNameSenderTV().setText("Me");
        } else {
            holder.getNameSenderTV().setText(item.getLastMessageSenderName());
        }
        holder.getLastMessageTV().setText(item.getLastMessage());
        holder.getInboxCV().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(position, 100);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
