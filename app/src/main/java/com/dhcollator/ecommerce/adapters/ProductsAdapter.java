package com.dhcollator.ecommerce.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhcollator.ecommerce.R;
import com.dhcollator.ecommerce.dtos.ProductsDTO;
import com.dhcollator.ecommerce.interfaces.OnCallback;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.viewholder.ProductsViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by Lenovo on 6/25/2016.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsViewHolder> implements Constants {

    List<ProductsDTO> data;
    OnItemClick mListener;
    DisplayImageOptions options;
    Context context;

    public ProductsAdapter(List<ProductsDTO> data, OnItemClick pListener, Context context) {
        this.data = data;
        mListener = pListener;
        this.context = context;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(100))
                .build();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dashboard, parent, false);
        return new ProductsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductsViewHolder holder, final int position) {
        ProductsDTO item = data.get(position);
        holder.titleTV.setText(item.getName());
        holder.priceTV.setText(item.getPrice());
        holder.locationTV.setText(item.getLocation());

        String keyDp =(item.getId()+"_product");

       Log.d("URL", "" + S3_BASE_URL + keyDp+"_product");
        ImageLoader.getInstance().displayImage(S3_BASE_URL + keyDp, holder.getProductPicIV(), options);

        holder.productsDashboardContainerCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(position, 100);
            }
        });
        holder.chatTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(position, 101);
            }
        });

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

}
