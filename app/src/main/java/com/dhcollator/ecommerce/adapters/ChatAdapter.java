package com.dhcollator.ecommerce.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhcollator.ecommerce.R;
import com.dhcollator.ecommerce.response.UserChatResponse;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.DateTimeUtils;
import com.dhcollator.ecommerce.viewholder.ChatViewHolder;
import com.dhcollator.ecommerce.viewholder.MyAddsViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lenovo on 7/16/2016.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> implements Constants {

    List<UserChatResponse> data;
    int loggedInUserId;
    private Context localContext;
    boolean isLastMessageMine = true;
    private DateTimeUtils dateTimeUtils;
     DisplayImageOptions options;

    public ChatAdapter(Context context, List<UserChatResponse> data, int loggedInUserId) {
        this.data = data;
        localContext = context;
        this.loggedInUserId = loggedInUserId;
        dateTimeUtils = DateTimeUtils.getInstance(context);
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(100))
                .build();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_view_2, parent, false);
        return new ChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        UserChatResponse item = data.get(position);
        String dateText = null;
        Log.d("MESSAGE-here", "" + item.getText());

        if (data.get(position).getMessageTime() != null && data.get(position).getMessageTime() != 0) {
            long timestamp = data.get(position).getMessageTime();
            dateText=dateTimeUtils.getTimeDiffString(timestamp);
        }

        if (data != null && data.get(position) != null && data.get(position).getLastMessageSender() != null
                && item.getLastMessageSender().equalsIgnoreCase(String.valueOf(loggedInUserId))) {
//       if (!item.getLastMessageSender().equalsIgnoreCase(String.valueOf(loggedInUserId))) {

            String keyDp = loggedInUserId + "_dp";

            holder.getContainerIn().setVisibility(View.VISIBLE);
            holder.getContainerOut().setVisibility(View.GONE);
            holder.getMeTime().setVisibility(View.VISIBLE);
            holder.getYourTime().setVisibility(View.GONE);
            holder.getMessageIn().setText(item.getText());
            holder.getMeTime().setText(dateText);
            ImageLoader.getInstance().displayImage(S3_BASE_URL+keyDp,holder.getMeIV(),options);

        } else {

            String keyDp = item.getLastMessageSender() + "_dp";

            holder.getContainerIn().setVisibility(View.GONE);
            holder.getContainerOut().setVisibility(View.VISIBLE);
            holder.getMeTime().setVisibility(View.GONE);
            holder.getYourTime().setVisibility(View.VISIBLE);
            holder.getFrom().setText(item.getSenderName());
            holder.getMessageOut().setText(item.getText());
            holder.getYourTime().setText(dateText);
            ImageLoader.getInstance().displayImage(S3_BASE_URL+keyDp,holder.getYourIV(),options);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
