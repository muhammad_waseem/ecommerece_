package com.dhcollator.ecommerce.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhcollator.ecommerce.R;
import com.dhcollator.ecommerce.dtos.MyAddsDTO;
import com.dhcollator.ecommerce.interfaces.OnCallback;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.viewholder.MyAddsViewHolder;
import com.dhcollator.ecommerce.viewholder.ProductsViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by Lenovo on 6/28/2016.
 */
public class MyAddsAdapter extends RecyclerView.Adapter<MyAddsViewHolder> implements Constants {

    List<MyAddsDTO> data;
    OnItemClick mListener;
    Context context;
    DisplayImageOptions options;

    public MyAddsAdapter(List<MyAddsDTO> data, OnItemClick pListener, Context context) {
        this.data = data;
        mListener = pListener;
        this.context=context;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(100))
                .build();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public MyAddsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_adds, parent, false);
        return new MyAddsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyAddsViewHolder holder, final int position) {
        MyAddsDTO item = data.get(position);
        String keyDp=item.getId()+"_product";

        holder.titleTV.setText(item.getName());
        holder.priceTV.setText(item.getPrice());
        holder.locationTV.setText(item.getLocation());
        ImageLoader.getInstance().displayImage(S3_BASE_URL + keyDp, holder.getMyAddPicIV(), options);

        holder.editIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(position,101);
            }
        });
        holder.deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(position,102);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
