package com.dhcollator.ecommerce.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhcollator.ecommerce.R;
import com.dhcollator.ecommerce.dtos.UserDTO1;


import java.util.List;

/**
 * Created by Lenovo on 6/13/2016.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersViewHolder> {

    List<UserDTO1> data;

    public UsersAdapter(List<UserDTO1> pAuctionItems) {
        data = pAuctionItems;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public UsersViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.users_row, viewGroup, false);
        return new UsersViewHolder(v);
    }

    @Override
    public void onBindViewHolder(UsersViewHolder holder, int position) {
        UserDTO1 item = data.get(position);
        holder.userFirstName.setText(item.getFirstName());
        holder.userLastName.setText(item.getLastName());
        holder.password.setText(item.getPassword());
        holder.email.setText(item.getEmail());
        holder.id.setText(item.getId());

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class UsersViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView userFirstName;
        TextView userLastName;
        TextView email;
        TextView id;
        TextView password;

        public UsersViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            userFirstName = (TextView) itemView.findViewById(R.id.tv_user_fname);
            userLastName = (TextView) itemView.findViewById(R.id.tv_user_lname);
            email = (TextView) itemView.findViewById(R.id.tv_user_email);
            password = (TextView) itemView.findViewById(R.id.tv_user_password);
            id = (TextView) itemView.findViewById(R.id.tv_user_id);
        }
    }
}
