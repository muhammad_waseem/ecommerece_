package com.dhcollator.ecommerce.network;


import android.content.Context;


import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.dhcollator.ecommerce.models.BaseModel;
import com.dhcollator.ecommerce.models.RegisterResponseModel;
import com.dhcollator.ecommerce.models.UserAuthRequest;
import com.dhcollator.ecommerce.response.EmptyResponse;
import com.dhcollator.ecommerce.response.ProductsResponseModel;
import com.dhcollator.ecommerce.response.UserResponseModel;
import com.dhcollator.ecommerce.utils.Constants;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Talha.Hafeez on 5/14/2015.
 */
public class NetworkOperations implements Constants {

    private static NetworkOperations mInstance;

    private NetworkOperations() {
    }

    public static NetworkOperations getInstance() {
        if (mInstance == null)
            mInstance = new NetworkOperations();
        return mInstance;
    }

    public <T> void getData(Context pContext, String action, String params, INetworkListener pNetworkListener, Class<T> ref) {

        IRequest request = new GetRequest(pContext, action, params);
        NetworkThread<T> networkThread = new NetworkThread<T>(pContext, ref, pNetworkListener);
        networkThread.execute(request);
    }

    public <T> void getData(Context pContext, String action, INetworkListener pNetworkListener, Class<T> ref) {
        getData(pContext, action, getEcomJSON(pContext), pNetworkListener, ref);
    }

    public <T> void postData(Context pContext, String action, Object requestDTO, INetworkListener pNetworkListener, Class<T> ref) {

        Gson gson = new Gson();
        String data = gson.toJson(requestDTO);

        IRequest request = new PostRequest(pContext, action, data);
        NetworkThread<T> networkThread = new NetworkThread<T>(pContext, ref, pNetworkListener);
        networkThread.execute(request);
    }


    public <T> void putData(Context pContext, String action, Object requestDTO, INetworkListener pNetworkListener, Class<T> ref) {

        Gson gson = new Gson();
        String data = gson.toJson(requestDTO);

        IRequest request = new PutRequest(pContext, action, data);
        NetworkThread<T> networkThread = new NetworkThread<T>(pContext, ref, pNetworkListener);
        networkThread.execute(request);
    }
    public <T> void deleteData1(Context pContext, String action, Object requestDTO, INetworkListener pNetworkListener, Class<T> ref) {

        Gson gson = new Gson();
        String data = gson.toJson(requestDTO);

        IRequest request = new DeleteRequest(pContext, action, data);
        NetworkThread<T> networkThread = new NetworkThread<T>(pContext, ref, pNetworkListener);
        networkThread.execute(request);
    }

    public <T> void deleteData(Context pContext, String action, String params, INetworkListener pNetworkListener, Class<T> ref) {

        IRequest request = new DeleteRequest(pContext, action, params);
        NetworkThread<T> networkThread = new NetworkThread<T>(pContext, ref, pNetworkListener);
        networkThread.execute(request);
    }

    public void getAllUsers1(Context pContext,INetworkListener<RegisterResponseModel> pNetworkListener){

        IRequest request = new GetRequest(pContext,ACTION_ALL_USERS, null);
        NetworkThread<RegisterResponseModel> thread = new NetworkThread<RegisterResponseModel>(pContext, RegisterResponseModel.class, pNetworkListener);
        thread.execute(request);

    }
    public void getAllUsers(Context pContext,INetworkListener<UserResponseModel> pNetworkListener){

        IRequest request = new GetRequest(pContext,ACTION_ALL_USERS, null);
        NetworkThread<UserResponseModel> thread = new NetworkThread<UserResponseModel>(pContext, UserResponseModel.class, pNetworkListener);
        thread.execute(request);

    }
    public void getAllProducts(Context pContext,INetworkListener<ProductsResponseModel> pNetworkListener){

        IRequest request = new GetRequest(pContext,ACTION_ALL_PRODUCTS, null);
        NetworkThread<ProductsResponseModel> thread = new NetworkThread<ProductsResponseModel>(pContext, ProductsResponseModel.class, pNetworkListener);
        thread.execute(request);

    }
    public void authenticateUser(Context pContext, UserAuthRequest user, INetworkListener<EmptyResponse> pNetworkListener) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("email", user.getUser());
            obj.put("password", user.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }

        IRequest request = new PostRequest(pContext,ACTION_AUTH_USER, obj.toString());
        NetworkThread<EmptyResponse> thread = new NetworkThread<EmptyResponse>(pContext, EmptyResponse.class, pNetworkListener);
        thread.execute(request);
    }

    public void registration(Context pContext, UserDTO1 user, INetworkListener<EmptyResponse> pNetworkListener){

        JSONObject obj = new JSONObject();
        try {
            obj.put("email", user.getEmail());
            obj.put("first_name", user.getFirstName());
            obj.put("last_name", user.getLastName());
            obj.put("age", user.getAge());
            obj.put("password", user.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }

        IRequest request = new PostRequest(pContext,ACTION_REGISTERATION, obj.toString());
        NetworkThread<EmptyResponse> thread = new NetworkThread<EmptyResponse>(pContext,EmptyResponse.class, pNetworkListener);
        thread.execute(request);
    }

    private String getEcomJSON(Context pContext, String patientKey) {

        return null;
    }
    private String getEcomJSON(Context pContext) {

        return getEcomJSON(pContext, "" + "");
    }
}
