package com.dhcollator.ecommerce.network;


import android.content.Context;
import android.util.Log;

import com.dhcollator.ecommerce.utils.Constants;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PostRequest implements IRequest {

    private String bodyData;
    private String action;
    Context context;

    public PostRequest(Context pContext,String pAction, String data) {
        bodyData = data;
        action = pAction;
        context = pContext;
    }

    @Override
    public String doRequest() {

        try {
            URL url = new URL(Constants.URL_BASE + action);
            Log.d("debug", "URL: " + Constants.URL_BASE + action);
            Log.d("debug", "BODY: " + bodyData);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            DataOutputStream dataout = new DataOutputStream(conn.getOutputStream());
            dataout.writeBytes(bodyData);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()), 8096);
            String response;
            StringBuilder resp = new StringBuilder();
            while ((response = in.readLine()) != null) {
                resp.append(response);
            }
            in.close();
            Log.d("debug", "response: " + resp.toString());
            return resp.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
