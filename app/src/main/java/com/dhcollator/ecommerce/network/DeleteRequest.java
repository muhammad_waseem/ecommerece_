package com.dhcollator.ecommerce.network;

import android.content.Context;
import android.util.Log;

import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.Utility;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DeleteRequest implements IRequest {

    private String mAction;
    private String mParams;
    Context context;

    public DeleteRequest(Context pContext, String action, String data) {
        context = pContext;
        mAction = action;
        mParams = data;
    }

    @Override
    public String doRequest() {

        String serverUrl = Constants.URL_BASE + mAction;
        if (mParams != null) {
          // serverUrl += "/" + mParams;
        }

        try {

            URL url = new URL(serverUrl);
            Log.d("debug", "URL: " + url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "json");

            Log.d("debug", "Response Code: " + conn.getResponseCode());
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String response = IOUtils.toString(in, "UTF-8");
            Log.d("debug", "Response: " + response);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
