package com.dhcollator.ecommerce.network;

public interface INetworkListener<T> {

	public void onPreExecute();
	public void onPostExecute(T result);
	public void doInBackground();
}
