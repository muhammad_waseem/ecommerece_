package com.dhcollator.ecommerce.network;

public interface IRequest {

	public String doRequest();
	
}
