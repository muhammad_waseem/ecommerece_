package com.dhcollator.ecommerce.network;

import android.content.Context;
import android.util.Log;

import com.dhcollator.ecommerce.utils.Constants;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetRequest implements IRequest {

    private String mAction;
    private String mParams;
    Context context;

    public GetRequest(Context pContext, String action, String data) {
        context = pContext;
        mAction = action;
        mParams = data;
    }

    @Override
    public String doRequest() {
        String serverUrl = Constants.URL_BASE + mAction;
        if (mParams != null) {
            serverUrl += "?" + mParams;
        }

        try {
            Log.d("debug", "URL: " + serverUrl);
            URL url = new URL(serverUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "json");
         //   conn.setRequestProperty("X-ASCCPE-USERNAME", "username");
           // conn.setRequestProperty("X-ASCCPE-PASSWORD", "password");

            Log.d("debug", "Response Code: " + conn.getResponseCode());
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String response = IOUtils.toString(in, "UTF-8");
            Log.d("debug", "Response: " + response.toString());

            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
