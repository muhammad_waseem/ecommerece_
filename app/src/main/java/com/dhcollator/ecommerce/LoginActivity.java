package com.dhcollator.ecommerce;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.dhcollator.ecommerce.models.BaseModel;
import com.dhcollator.ecommerce.models.UserAuthRequest;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.EmptyResponse;
import com.dhcollator.ecommerce.response.EmptyResponseDataEntity;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;
import com.dhcollator.ecommerce.utils.Utility;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends Activity implements View.OnClickListener, INetworkListener {

    private EditText mEmailET;
    private EditText mPasswordET;
    private Button mLoginBtn;
    String email, pwd;
    ImageView backIV;
    String emailPattern;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        resourceInit();
        progressDialog = new ProgressDialog(LoginActivity.this);
        mLoginBtn.setOnClickListener(this);
        backIV.setOnClickListener(this);
    }

    private void resourceInit() {
        mEmailET = (EditText) findViewById(R.id.et_login_email);
        mPasswordET = (EditText) findViewById(R.id.et_login_password);
        mLoginBtn = (Button) findViewById(R.id.btn_login_);
        backIV = (ImageView) findViewById(R.id.iv_sigin_back);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login_:
                login();
                break;
            case R.id.iv_sigin_back:
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
                break;
        }
    }

    private void login() {
        email = mEmailET.getText().toString();
        pwd = mPasswordET.getText().toString();
        if (isValidInput()) {
            UserAuthRequest request = new UserAuthRequest();
            request.setUser(email);
            request.setPassword(pwd);
            NetworkOperations.getInstance().authenticateUser(this, request, this);
        }
    }

    private boolean isValidInput() {
        boolean result = true;
        email = mEmailET.getText().toString();
        pwd = mPasswordET.getText().toString();
        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (!TextUtils.isEmpty(email)) {
            if (email.matches(emailPattern)) {
            } else {
                mEmailET.setError("please enter valid email");
                result = false;
            }
        } else {
            mEmailET.setError("Mandatory field");
            result = false;
        }
        if (TextUtils.isEmpty(pwd)) {
            mPasswordET.setError("this field is required");
            result = false;
        }

        return result;
    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("Authenticating");
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object obj) {
        progressDialog.cancel();

        if (obj instanceof EmptyResponse) {

            EmptyResponse result = (EmptyResponse) obj;
            if (result != null && result.isSuccess()) {

                int userId = result.getData().get(0).getId();
                String email = result.getData().get(0).getEmail();
                String firstName = result.getData().get(0).getFirst_name();
                String lastName = result.getData().get(0).getFirst_name();

                SharedPreferenceUtils.getInstance(LoginActivity.this).setUserId(userId);
                SharedPreferenceUtils.getInstance(LoginActivity.this).setUserFirstName(firstName);
                SharedPreferenceUtils.getInstance(LoginActivity.this).setUserLastName(lastName);
                SharedPreferenceUtils.getInstance(LoginActivity.this).setUserEmail(email);
                Intent intent = new Intent(LoginActivity.this, Dashboard.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(LoginActivity.this, "Login Faild",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void doInBackground() {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
