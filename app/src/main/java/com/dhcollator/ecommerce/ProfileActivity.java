package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.dhcollator.ecommerce.adapters.UsersAdapter;
import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.EmptyResponse;
import com.dhcollator.ecommerce.response.MyAddsResponse;
import com.dhcollator.ecommerce.response.UserResponseModel;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity implements INetworkListener, View.OnClickListener, Constants {

    TextView firstNameTV;
    TextView lastNameTV;
    TextView emailTV;
    TextView passwordTV;
    ImageView profileCovrIV;
    ImageView profileDP;
    private static int RESULT_LOAD_GALLERY = 1;
    private static int RESULT_LOAD_CAMERA = 0;
    private ProgressDialog progressDialog;
    boolean isCover = true;
    DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setTitle("Profile");
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(ProfileActivity.this));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(10))
                .build();
        firstNameTV = (TextView) findViewById(R.id.tv_profile_fname);
        lastNameTV = (TextView) findViewById(R.id.tv_profile_lname);
        emailTV = (TextView) findViewById(R.id.tv_profile_email);
        profileCovrIV = (ImageView) findViewById(R.id.iv_profile_cover);
        profileDP = (ImageView) findViewById(R.id.iv_profile_dp);
        profileCovrIV.setOnClickListener(this);
        profileDP.setOnClickListener(this);

        progressDialog = new ProgressDialog(ProfileActivity.this);
        int userId = SharedPreferenceUtils.getInstance(ProfileActivity.this).getUserId();
        String action = Constants.ACTION_GET_MY_PROFILE + "/" + userId;
        NetworkOperations.getInstance().getData(ProfileActivity.this, action, null, this, UserResponseModel.class);
        getProfilePicAndCoverPics();
    }

    public void getProfilePicAndCoverPics() {

        int loggedInUserId = SharedPreferenceUtils.getInstance(ProfileActivity.this).getUserId();
        String keyCoverPic = loggedInUserId + "_cover";
        String keyDp = loggedInUserId + "_dp";

        ImageLoader.getInstance().displayImage(S3_BASE_URL + keyDp, profileDP, options);
        ImageLoader.getInstance().displayImage(S3_BASE_URL + keyCoverPic, profileCovrIV, options);
    }

    @Override
    public void onPreExecute() {
        progressDialog.setMessage("fetching data...");
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object obj) {
        progressDialog.cancel();
        if (obj instanceof UserResponseModel) {

            UserResponseModel result = (UserResponseModel) obj;
            if (result.isSuccess()) {
                firstNameTV.setText(result.getData().get(0).getFirstName());
                lastNameTV.setText(result.getData().get(0).getLastName());
                emailTV.setText(result.getData().get(0).getEmail());
            }
        }

    }

    @Override
    public void doInBackground() {

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ProfileActivity.this, Dashboard.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_profile_cover:
                isCover = true;
                selectImage();
                break;
            case R.id.iv_profile_dp:
                isCover = false;
                selectImage();
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Choose from Gallery", "Take Picture",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        if (isCover) {
            builder.setTitle("Add Cover Photo");
        } else {
            builder.setTitle("Select your DP");
        }
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, RESULT_LOAD_GALLERY);
                } else if (items[item].equals("Take Picture")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, RESULT_LOAD_CAMERA);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_CAMERA && resultCode == RESULT_OK && null != data) {
            Bitmap image = (Bitmap) data.getExtras().get("data");

            profileCovrIV.setImageBitmap(image);

        }

        if (requestCode == RESULT_LOAD_GALLERY && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            Log.d("Upload-File-Path", ":" + picturePath);
            uploadFileToS3(picturePath);
            if (isCover) {
                profileCovrIV.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            } else {
                profileDP.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            }
        }
    }

    private void uploadFileToS3(String filePath) {
        AWSCredentials credential = new BasicAWSCredentials(S3_ACCESS_KEY_ID, S3_SECRET_KEY);
        AmazonS3 s3 = new AmazonS3Client(credential);
        TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());
        String key = null;
        int loggedInUserId = SharedPreferenceUtils.getInstance(ProfileActivity.this).getUserId();
        if (isCover) {
            key = loggedInUserId + "_cover";
        } else {
            key = loggedInUserId + "_dp";
        }
        TransferObserver observer1 = transferUtility.upload(
                S3_BUCKET_NAME,
                key,
                new File(filePath)
        );

    }
}
