package com.dhcollator.ecommerce.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhcollator.ecommerce.R;
import com.dhcollator.ecommerce.adapters.ProductsAdapter;
import com.dhcollator.ecommerce.dtos.ProductsDTO;
import com.dhcollator.ecommerce.interfaces.OnCallback;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.network.INetworkListener;
import com.dhcollator.ecommerce.network.NetworkOperations;
import com.dhcollator.ecommerce.response.ProductsEntityResponseModel;
import com.dhcollator.ecommerce.response.ProductsResponseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsFragment extends Fragment implements View.OnClickListener, INetworkListener,OnCallback,OnItemClick {


    RecyclerView productsRV;
    ProductsAdapter productsAdapter;
    List<ProductsDTO> data = new ArrayList<>();

    public ProductsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        productsRV = (RecyclerView) view.findViewById(R.id.rv_products_all);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        productsRV.setLayoutManager(llm);
        NetworkOperations.getInstance().getAllProducts(getActivity(), this);
        productsAdapter = new ProductsAdapter(data,this,getActivity());

        return view;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPreExecute() {

    }

    @Override
    public void onPostExecute(Object obj) {
        if (obj instanceof ProductsResponseModel){
            ProductsResponseModel result=(ProductsResponseModel)obj;
            if (result.isSuccess()) {
                Log.d("Success", "" + result.isSuccess());
                for (int i = 0; i < result.getProducts().size(); i++) {
                    ProductsEntityResponseModel currentEntity = result.getProducts().get(i);
                    ProductsDTO productsDTO=new ProductsDTO();
                    productsDTO.setId(currentEntity.getId());
                    productsDTO.setName(currentEntity.getName());
                    productsDTO.setCode(currentEntity.getCode());
                    productsDTO.setCreated_at(currentEntity.getCreated_at());
                    productsDTO.setStatus(currentEntity.getStatus());
                    data.add(productsDTO);
                    productsRV.setAdapter(productsAdapter);

                }
            }
        }
    }

    @Override
    public void doInBackground() {

    }

    @Override
    public void onClickCallback() {

    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onClick(int pPosition, int id) {

    }
}
