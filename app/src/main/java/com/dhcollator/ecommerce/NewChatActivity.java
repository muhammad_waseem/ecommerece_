package com.dhcollator.ecommerce;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dhcollator.ecommerce.adapters.ChatAdapter;
import com.dhcollator.ecommerce.adapters.MyAddsAdapter;
import com.dhcollator.ecommerce.dtos.ProductsDTO;
import com.dhcollator.ecommerce.dtos.UserDTO1;
import com.dhcollator.ecommerce.response.UserChatResponse;
import com.dhcollator.ecommerce.utils.Constants;
import com.dhcollator.ecommerce.utils.SharedPreferenceUtils;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewChatActivity extends AppCompatActivity implements View.OnClickListener, Constants {

    EditText messageTextET;
    ImageView sendBTN;
    RecyclerView chatRV;
    Firebase ref, ref1, ref2;
    ChatAdapter chatAdapter;
    UserDTO1 friendsDetailData;
    String chatRoomId;
    int loggedInUserId;
    private ProgressDialog progressDialog;
    String text;
    private ArrayList<UserChatResponse> chatUsers = new ArrayList<UserChatResponse>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_chat);
        Firebase.setAndroidContext(this);
        progressDialog = new ProgressDialog(NewChatActivity.this);

        friendsDetailData = (UserDTO1) getIntent().getSerializableExtra("detail");

        messageTextET = (EditText) findViewById(R.id.et_new_chat_message_text);
        chatRV = (RecyclerView) findViewById(R.id.rv_listChat);
        LinearLayoutManager llm = new LinearLayoutManager(NewChatActivity.this);
        chatRV.setLayoutManager(llm);

        loggedInUserId = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserId();
        chatAdapter = new ChatAdapter(NewChatActivity.this, chatUsers, loggedInUserId);
        chatRV.setAdapter(chatAdapter);
        sendBTN = (ImageView) findViewById(R.id.iv_new_chat_send);
        sendBTN.setOnClickListener(this);
        fetchPreviousChat();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_new_chat_send:
                sendMessage();
                break;
        }

    }

    public void sendMessage() {
        String text = messageTextET.getText().toString();
        text = text.trim();
        if (text == null || text.equalsIgnoreCase("") || text.length() <= 0) {
            Toast.makeText(NewChatActivity.this, "Message cannot be empty", Toast.LENGTH_LONG).show();
            return;
        } else {
            pushMessage();
            messageTextET.setText("");
        }
    }

    public void pushMessage() {
        text = messageTextET.getText().toString();
        usersInfoAtFirbase();
        int loggedInUserId = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserId();
        String loggedInUserFirstName = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserFirstName();
        int friendId = Integer.parseInt(friendsDetailData.getId());
        if (loggedInUserId < friendId) {
            chatRoomId = loggedInUserId + "-" + friendsDetailData.getId();
        } else {
            chatRoomId = friendsDetailData.getId() + "-" + loggedInUserId;
        }
        chatRooms();
        chatRoomInfo();
        chatRoomsMembersDetail();

        ref1 = new Firebase(FIREBASE_URL + "/" + CHATROOMS + "/" + chatRoomId + "/" + MESSAGES).push();
        ref1.child("chat_id").setValue(chatRoomId);
        ref1.child("text").setValue(text);
        ref1.child("lastMessageSender").setValue(loggedInUserId);
        ref1.child("senderName").setValue(loggedInUserFirstName);
        ref1.child("messageTime").setValue(System.currentTimeMillis());
        getData();
    }

    public void getData() {
        Firebase ref3 = new Firebase(FIREBASE_URL + "/" + CHATROOMS + "/" + chatRoomId + "/" + MESSAGES);
        ref3.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                // Log.d("Chat_JSON-1", "" + dataSnapshot.getValue());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //  Log.d("Chat_JSON-2", "" + dataSnapshot.getValue());


                if (dataSnapshot != null) {
                    try {
                        @SuppressWarnings("rawtypes")
                        final Map map = (Map) dataSnapshot.getValue();
                        if (map != null) {
                            JSONObject o = new JSONObject(map);
                            long timeSpan = o.getLong("messageTime");
                            Log.d("Chat_JSON-2", "" + timeSpan);
                            Gson gson = new Gson();
                            UserChatResponse chatUser = gson.fromJson(o.toString(), UserChatResponse.class);
                            if (!chatUsers.contains(chatUser)) {
                                chatUsers.add(chatUser);
                                chatAdapter.notifyDataSetChanged();
                            }
                            pushNotification(chatUser.getText());
                        }
                    } catch (Exception e) {
                        Log.e("Chat", e.getMessage());
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void usersInfoAtFirbase() {
        ref = new Firebase(FIREBASE_URL + "/" + USERS);
        Map<String, String> map = new HashMap<String, String>();
        map.put("first_name", friendsDetailData.getFirstName());
        map.put("last_name", friendsDetailData.getLastName());
        map.put("email", friendsDetailData.getEmail());
        map.put("id", friendsDetailData.getId());
        ref.child(friendsDetailData.getId() + "").setValue(map);
        map.clear();

        String loggedInUserFirstName = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserFirstName();
        String loggedInUserLastName = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserLastName();

        map.put("first_name", loggedInUserFirstName);
        map.put("last_name", loggedInUserLastName);
        ref.child(loggedInUserId + "").setValue(map);
    }

    public void chatRooms() {
        int loggedInUserId = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserId();
        int friendId = Integer.parseInt(friendsDetailData.getId());
        ref = new Firebase(FIREBASE_URL + "/" + MYROOMS + "/" + loggedInUserId);
        if (loggedInUserId < friendId) {
            chatRoomId = loggedInUserId + "-" + friendsDetailData.getId();
        } else {
            chatRoomId = friendsDetailData.getId() + "-" + loggedInUserId;
        }
        ref = new Firebase(FIREBASE_URL + "/" + MYROOMS + "/" + loggedInUserId + "/" + chatRoomId);
        ref.child("chat_id").setValue(chatRoomId);
    }

    public void chatRoomInfo() {
        int loggedInUserId = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserId();
        String loggedInUserLastName = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserLastName();
        ref = new Firebase(FIREBASE_URL + "/" + CHATROOMS + "/" + chatRoomId + "/" + INFO);
        ref.child("chat_id").setValue(chatRoomId);
        ref.child("createTimestemp").setValue(System.currentTimeMillis());
        ref.child("lastMessage").setValue(text);
        ref.child("lastMessageSender").setValue(loggedInUserId);
        ref.child("lastMessageSenderName").setValue(loggedInUserLastName);
        ref.child("lastMessageDate").setValue(System.currentTimeMillis());
    }

    public void chatRoomsMembersDetail() {

        int loggedInUserId = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserId();
        String loggedInUserFirstName = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserFirstName();
        ref = new Firebase(FIREBASE_URL + "/" + CHATROOMS + "/" + chatRoomId + "/" + INFO + "/" + MEMBERS + "/" + friendsDetailData.getId());
        ref.child("id").setValue(friendsDetailData.getId());
        ref.child("name").setValue(friendsDetailData.getFirstName());

        ref = new Firebase(FIREBASE_URL + "/" + CHATROOMS + "/" + chatRoomId + "/" + INFO + "/" + MEMBERS + "/" + loggedInUserId);
        ref.child("id").setValue(loggedInUserId);
        ref.child("name").setValue(loggedInUserFirstName);
    }

    public void fetchPreviousChat() {

        progressDialog.setMessage("Loading Messages ...");
        progressDialog.show();

        int loggedInUserId = SharedPreferenceUtils.getInstance(NewChatActivity.this).getUserId();
        int friendId = Integer.parseInt(friendsDetailData.getId());
        if (loggedInUserId < friendId) {
            chatRoomId = loggedInUserId + "-" + friendsDetailData.getId();
        } else {
            chatRoomId = friendsDetailData.getId() + "-" + loggedInUserId;
        }
        Firebase previousChatPath = new Firebase(FIREBASE_URL + "/" + CHATROOMS + "/" + chatRoomId + "/" + MESSAGES);
        Query getAllMessages = previousChatPath;
        getAllMessages.addListenerForSingleValueEvent(listener);
    }

    private ValueEventListener listener = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot snap) {
            progressDialog.cancel();
            if (snap != null) {
                Object value = snap.getValue();
                @SuppressWarnings("rawtypes")
                final Map map = (Map) value;
                if (map != null) {
                    for (Object key : map.keySet()) {
                        @SuppressWarnings("unchecked")
                        Map<Object, Object> obj = (Map<Object, Object>) map.get(key);
                        JSONObject o = new JSONObject(obj);
                   //     Log.d("Debug-Messages", ":" + o.toString());
                        Gson gson = new Gson();
                        UserChatResponse chatUser = gson.fromJson(o.toString(), UserChatResponse.class);
                        if (chatUsers.indexOf(chatUser) == -1) {
                            chatUsers.add(chatUser);
                        }
                        chatAdapter.notifyDataSetChanged();
                    }

                }

            }
        }

        @Override
        public void onCancelled(FirebaseError arg0) {

        }
    };
public void pushNotification(String message){

    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put("first_name", friendsDetailData.getFirstName());
    map.put("last_name", friendsDetailData.getLastName());
    map.put("email", friendsDetailData.getEmail());
    map.put("id", friendsDetailData.getId());

    Intent intent = new Intent();
    intent.setAction("com.dhcollator.ecommerce.receiver.NotificationRecevier");
    intent.putExtra("message",message);
    intent.putExtra("map",map.toString());
    Log.d("HERE IS map",""+map.toString());
    sendBroadcast(intent);
}
}
