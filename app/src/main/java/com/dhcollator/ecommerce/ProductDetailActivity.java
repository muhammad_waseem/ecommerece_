package com.dhcollator.ecommerce;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhcollator.ecommerce.dtos.ProductsDTO;
import com.dhcollator.ecommerce.interfaces.OnItemClick;
import com.dhcollator.ecommerce.utils.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener, Constants {

    TextView titleTV;
    TextView priceTV;
    TextView locationTV;
    TextView brandTV;
    TextView yearTV;
    TextView conditionTV;
    TextView contactTV;
    TextView detailTV;
    Button nextBTN;
    ImageView bannerIV;
    DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(ProductDetailActivity.this));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(10))
                .build();
        titleTV = (TextView) findViewById(R.id.tv_product_detail_title);
        priceTV = (TextView) findViewById(R.id.tv_product_detail_price);
        locationTV = (TextView) findViewById(R.id.tv_product_detail_location);
        brandTV = (TextView) findViewById(R.id.tv_product_detail_brand);
        yearTV = (TextView) findViewById(R.id.tv_product_detail_year);
        conditionTV = (TextView) findViewById(R.id.tv_product_detail_condition);
        contactTV = (TextView) findViewById(R.id.tv_product_detail_contact);
        detailTV = (TextView) findViewById(R.id.tv_product_detail_detail);
        nextBTN = (Button) findViewById(R.id.btn_detail_product_next);
        bannerIV = (ImageView) findViewById(R.id.iv_product_detail_image);

        nextBTN.setOnClickListener(this);
        ProductsDTO detailData = (ProductsDTO) getIntent().getParcelableExtra("detail");
        titleTV.setText(detailData.getName());
        priceTV.setText(detailData.getPrice());
        locationTV.setText(detailData.getLocation());
        brandTV.setText(detailData.getBrand());
        yearTV.setText(detailData.getCreated_at());
        conditionTV.setText(detailData.getPresent_condition());
        contactTV.setText(detailData.getContact());
        detailTV.setText(detailData.getDetail());

        String keyDp = detailData.getId() + "_product";
        Log.d("URL", "" + S3_BASE_URL + keyDp);
        ImageLoader.getInstance().displayImage(S3_BASE_URL + keyDp, bannerIV, options);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ProductDetailActivity.this, Dashboard.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_detail_product_next:
                Log.d("Clicked", "here");
                break;
        }
    }
}
